FROM node:16

ENV PORT 3000

WORKDIR /usr/src/app

COPY package.json /usr/src/app/

RUN npm install -g npm@8.1.3
RUN npm install

COPY . /usr/src/app
# Building app
EXPOSE 3000
# Running the app
CMD "./script.sh"
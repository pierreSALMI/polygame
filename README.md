# Projet Poly Game

[insérer description du Ydays]

## Installation

1. Clonez ce projet dans un dossier
2. Installez Docker et Docker-Compose si ce n'est pas déjà fait
3. script.sh à besoin de plus de droit pour s'executer:

```shell
chmod +x script.sh
```

4. À l'intérieur de ce dossier, tapez ces commandes :

```shell
docker-compose build
```

```shell
docker-compose up -d
```

5. À partir d'ici vous pouvez aller sur votre navigateur préféré et taper dans l'url afin de consulter tous les requêtes existantes sur le site :
```
localhost:3000
```
